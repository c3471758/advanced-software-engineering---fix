﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment_Test2
{
    /// <summary>
    /// Shape object used to set and get the values of user-defined variables.  
    /// </summary>
    class ShapeObject
    {
        public string valName { get; set; }
        public int val { get; set; }

        public ShapeObject(string valName, int val)
        {
            this.valName = valName;
            this.val = val;
        }

        public String getValName()
        {
            return this.valName;
        }

        public void setValName(String valName)
        {
            this.valName = valName;
        }

        public int getVal()
        {
            return this.val;
        }

        public void setVal(int val)
        {
            this.val = val;

        }
    }
}

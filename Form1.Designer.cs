﻿namespace ASE_Assignment_Test2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommandLine = new System.Windows.Forms.TextBox();
            this.CommandList = new System.Windows.Forms.RichTextBox();
            this.Output = new System.Windows.Forms.PictureBox();
            this.runBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Output)).BeginInit();
            this.SuspendLayout();
            // 
            // CommandLine
            // 
            this.CommandLine.Location = new System.Drawing.Point(12, 536);
            this.CommandLine.Name = "CommandLine";
            this.CommandLine.Size = new System.Drawing.Size(304, 20);
            this.CommandLine.TabIndex = 0;
            this.CommandLine.TextChanged += new System.EventHandler(this.CommandLine_TextChanged);
            // 
            // CommandList
            // 
            this.CommandList.Location = new System.Drawing.Point(12, 12);
            this.CommandList.Name = "CommandList";
            this.CommandList.Size = new System.Drawing.Size(304, 518);
            this.CommandList.TabIndex = 1;
            this.CommandList.Text = "";
            // 
            // Output
            // 
            this.Output.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Output.Location = new System.Drawing.Point(328, 13);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(1065, 573);
            this.Output.TabIndex = 2;
            this.Output.TabStop = false;
            // 
            // runBtn
            // 
            this.runBtn.Location = new System.Drawing.Point(12, 562);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(304, 24);
            this.runBtn.TabIndex = 3;
            this.runBtn.Text = "Run";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1405, 598);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.CommandList);
            this.Controls.Add(this.CommandLine);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Output)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CommandLine;
        private System.Windows.Forms.RichTextBox CommandList;
        private System.Windows.Forms.PictureBox Output;
        private System.Windows.Forms.Button runBtn;
    }
}


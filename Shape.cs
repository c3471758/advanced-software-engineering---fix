﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment_Test2
{
    /// <summary>
    /// Shape class, defines the different available shape objects.
    /// </summary>
    public class Shape
    {
        /// <summary>
        /// List of accepted shapes.
        /// </summary>
        public enum Shapes
        {
            Circle,
            Rectangle,
            Line,
            Triangle
        }

        /// <summary>
        /// Shape constructor.
        /// </summary>
        public Shape()
        { }

        /// <summary>
        /// Shape constructor 2 sides.
        /// </summary>
        /// <param name="shape">Shape name</param>
        /// <param name="width">Shape width</param>
        /// <param name="height">Shape height</param>
        public Shape(Shapes shape, int width, int height)
        {
            ShapeName = shape;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Shape constructor 3 sides.
        /// </summary>
        /// <param name="shape">Shape name</param>
        /// <param name="width">Shape width</param>
        /// <param name="height">Shape height</param>
        /// <param name="side3">Shape side3 for polygons</param>
        public Shape(Shapes shape, int width, int height, int side3)
        {
            ShapeName = shape;
            Width = width;
            Height = height;
            Side3 = side3;
        }

        /// <summary>
        /// Shape and paramaters getters and setters.
        /// </summary>
        public Shapes ShapeName { get; set; } = Shapes.Circle;
        public int Width { get; set; }
        public int Height { get; set; }
        public int Side3 { get; set; }

        /// <summary>
        /// Validates shape commands
        /// </summary>
        /// <param name="input">Input string</param>
        /// <param name="result">Shape output</param>
        /// <returns></returns>
        public static bool TryParse(string input, out Shape result)
        {
            result = null;

            if (string.IsNullOrEmpty(input))
                return false;

            var parts = input.Split(' ');
            var validParts = Enum.GetNames(typeof(Shapes));

            if (!validParts.Contains(validParts[0], StringComparer.OrdinalIgnoreCase))
                return false;

            int w, h, side3;

            if (!int.TryParse(parts[1], out w) ||
                !int.TryParse(parts[2], out h) ||
                !int.TryParse(parts[3], out side3))
                return false;

            if (w <= 0 || h <= 0)
                return false;

            var shape = (Shapes)validParts.ToList().FindIndex(a => a.Equals(parts[0], StringComparison.OrdinalIgnoreCase));

            result = new Shape(shape, w, h, side3);
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections;

namespace ASE_Assignment_Test2
{ 
    /// <summary>
    /// Form1 Class.
    /// </summary>
    public partial class Form1 : Form
    {
        String command;
        Graphics g;

        int x, y;

        Shape shape;

        Bitmap drawShape;

        public int firstParam = 0;

        List<ShapeObject> variables = new List<ShapeObject>();

        /// <summary>
        /// Form1 Constructor, sets drawing area.
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            drawShape = new Bitmap(Output.Size.Width, Output.Size.Height);
            Output.Image = drawShape;

        }

        /// <summary>
        /// Sets the command variable, to the value of the Command Line in the GUI.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandLine_TextChanged(object sender, EventArgs e)
        {
            command = CommandLine.Text;
        }

        /// <summary>
        /// When the run button is clicked, search for the appropriate commands (None shape or pen commands).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void runBtn_Click(object sender, EventArgs e)
        {
            // Path to store the saved text file.
            String path = @".\commands.txt";

            // Multi-line command.
            // Takes each command from the textbox and moves to the command parser.
            if (command == "run")
            {
                var lines = CommandList.Lines;

                foreach (var line in lines)
                {
                    Console.WriteLine(line);

                    command = line;
                    commandParser();
                }
            }
            // Clears drawing area to original, default settings.
            else if (command == "clear")
            {
                g = Graphics.FromImage(drawShape);
                g.Clear(Color.WhiteSmoke);
                g.Dispose();
                Output.Refresh();
            }
            else if (command == "save")
            {
                // Saves the contents of the CommandList rich text box to a file in the project directory
                CommandList.SaveFile(path, RichTextBoxStreamType.PlainText);
            }
            else if (command == "load")
            {
                // Reads in a previously saved commands file from file.  
                // Puts contents of file into a list
                List<string> lines = System.IO.File.ReadLines(path).ToList();

                // Puts contents of file into the CommandList richtextbox, as well as adding a new line per command.  
                foreach (string line in lines)
                {
                    CommandList.Text += line + "\n";
                }
            }
            // Resets the x and y co-ordinates to default (top left corner of the drawing window)
            else if (command == "reset")
            {
                x = 0;
                y = 0;

                Console.WriteLine(x);
                Console.WriteLine(y);

            } 
            else
            {
                commandParser();
            }

        }

        /// <summary>
        /// The shapeParser class takes a string input and outputs shapes onto the canvas, based on user paramaters.
        /// </summary>
        public void shapeParser()
        {   
            // Parse shape
            shape = null;

            if (!Shape.TryParse(command, out shape))
            {
                CommandLine.Text = "Enter a valid shape command.";
            }

            Output.Invalidate();

            // Prepare bitmap assignment
            g = Graphics.FromImage(drawShape);

            if (shape != null)
            {
                // Use pen
                using (Pen pn = new Pen(Color.Black, 5))
                {
                    // If input is as expected continue
                    try
                    {
                        // Case for each shape
                        switch (shape.ShapeName)
                        {
                            case Shape.Shapes.Circle:
                                g.DrawEllipse(pn, x, y, shape.Width, shape.Height);
                                processDraw();
                                break;
                            case Shape.Shapes.Rectangle:
                                g.DrawRectangle(pn, x, y, shape.Width, shape.Height);
                                processDraw();
                                break;
                            case Shape.Shapes.Line:
                                g.DrawLine(pn, x, y, shape.Width, shape.Height);
                                processDraw();
                                break;
                            case Shape.Shapes.Triangle:

                                PointF point1 = new PointF(50.0f, shape.Width);
                                PointF point2 = new PointF(100.0f, shape.Height);
                                PointF point3 = new PointF(150.0f, shape.Side3);

                                PointF[] polygonPoints = { point1, point2, point3 };

                                g.DrawPolygon(pn, polygonPoints);
                                processDraw();
                                break;
                        }
                    } catch
                    {
                        CommandLine.Text = "Enter a valid shape command.";
                    }

                }
            }
        }

        /// <summary>
        /// The command parser contains the moveTo command, as well as the loop and variable assignment commands.
        /// </summary>
        private void commandParser()
        {
            string[] parts = command.Split(' ');

            if (parts[0] == "moveTo")
            {
                // Check if coordinates are numeric, if they are set X & Y coordinates for shape.
                if (parts.Length != 3)
                {
                    CommandLine.Text = "Syntax error: Please format like (moveTo X Y)";
                } else
                {
                    if (Regex.IsMatch(parts[1], @"^\d+$") && Regex.IsMatch(parts[2], @"^\d+$") == true)
                    {
                        x = Int32.Parse(parts[1]);
                        y = Int32.Parse(parts[2]);
                    }
                    else
                    {
                        CommandLine.Text = "Syntax error: Please enter numerical co-ordinates";
                    }
                }
                Console.WriteLine("X value: " + x + " Y value: " + y);
            }
            else if (parts[0] == "loop")
            {
                try
                {
                    int loop = Int32.Parse(parts[1]);
                    Console.WriteLine("Looping...");
                    int i = 0;
                    string[] lines = CommandList.Lines;
                    int lineCounter = 0;

                    while (i != loop)
                    {
                        for (int j = 1; j < lines.Length; j++)
                        {
                            Console.WriteLine(j);
                            lineCounter++;

                            command = lines[j];
                            commandParser();
                        }
                        Console.WriteLine(i);
                        i++;
                    }

                }
                catch (Exception ex)
                {
                   
                }
            }
            else
            {
                for (int i = 0; i < variables.Count; i++)
                {
                    // x = 30
                    // circle x
                    if (variables[i].getValName() == parts[1])
                    {
                        firstParam = variables[i].getVal();
                        //Console.WriteLine("PARTS 0: " + parts[1]);
                        Console.WriteLine("FIRST PARAM: " + firstParam);
                    }

                }

                // If no other commands are present, do shape parse / draw

                shapeParser();
            }
        }

        /// <summary>
        /// The processDraw method updates the canvas with the desired shapes.
        /// </summary>
        private void processDraw()
        {
            g = Graphics.FromImage(drawShape);
            Output.Image = drawShape;
            g.Dispose();
        }

    }
}
